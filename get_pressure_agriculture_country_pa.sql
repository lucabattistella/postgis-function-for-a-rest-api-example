-- Function: pressures.get_pressure_agriculture_country_pa(text, text, integer)

-- DROP FUNCTION pressures.get_pressure_agriculture_country_pa(text, text, integer);

CREATE OR REPLACE FUNCTION pressures.get_pressure_agriculture_country_pa(
    IN a_iso3 text DEFAULT NULL::text,
    IN b_iso2 text DEFAULT NULL::text,
    IN c_un_m49 integer DEFAULT NULL::integer)
  RETURNS TABLE(country_rank integer, wdpaid integer, iso3 text, iso2 text, un_m49 integer, ap numeric, normalized_ap numeric, normalized_avg_ap numeric) AS
$BODY$


DECLARE
   sql TEXT;
BEGIN

sql :='
WITH
unnested AS (
SELECT * FROM protected_sites.get_wdpa_country_codes_unnested()
),
press_atts AS (
SELECT
a.*,
b.ind
FROM unnested a
LEFT JOIN 
'||
(SELECT 'pressures.'||table_name FROM information_schema.tables
WHERE table_schema='pressures' AND table_name LIKE 'agriculture_pa_%'
ORDER BY table_name DESC
LIMIT 1)
||' b
ON a.wdpaid=b.wdpaid
WHERE b.ind IS NOT NULL
ORDER BY wdpaid),
range_country AS (
SELECT
iso3,
MAX(ind) max_ind,
MIN(ind) min_ind,
MAX(ind)-MIN(ind) range_country,
COUNT(wdpaid) count_wdpaid
FROM press_atts
GROUP BY iso3
ORDER by iso3),

normalized AS (
SELECT
a.*,
CASE
WHEN b.min_ind=b.max_ind AND a.ind !=0 THEN 100.00
WHEN b.min_ind=b.max_ind AND a.ind =0 THEN 0.00
ELSE ROUND(100*((a.ind-b.min_ind)/b.range_country)::numeric,2)
END normalized_ind
FROM press_atts a
JOIN range_country b ON a.iso3=b.iso3
),
normalized_avg AS (
SELECT
iso3,
ROUND(AVG(normalized_ind)::numeric,2) normalized_avg_ind
FROM normalized
GROUP BY iso3
ORDER BY iso3),

final_pressure AS (
SELECT
(rank() over (PARTITION BY a.iso3 ORDER BY normalized_ind,wdpaid))::integer as country_rank,
a.wdpaid,
a.iso3,
a.iso2::text,
a.un_m49,
a.ind,
a.normalized_ind,
b.normalized_avg_ind
FROM normalized a
JOIN normalized_avg b ON a.iso3=b.iso3
ORDER BY iso3,country_rank)

SELECT
country_rank,
wdpaid,
iso3,
iso2,
un_m49,
ind,
normalized_ind,
normalized_avg_ind
FROM final_pressure';

IF a_iso3 IS NOT NULL THEN
			sql := sql || ' WHERE iso3 = $1';
ELSE	sql := sql || ' WHERE iso3 IS NOT NULL';
END IF;

IF b_iso2 IS NOT NULL THEN
			sql := sql || ' AND iso2 = $2';
ELSE	sql := sql || ' AND iso2 IS NOT NULL';
END IF;

IF c_un_m49 IS NOT NULL THEN
			sql := sql || ' AND un_m49 = $3';
ELSE	sql := sql || ' AND un_m49 IS NOT NULL';
END IF;

sql := sql || ';';

RETURN QUERY EXECUTE sql USING a_iso3,b_iso2,c_un_m49;
END;

$BODY$
  LANGUAGE plpgsql IMMUTABLE
  COST 100
  ROWS 1000;
ALTER FUNCTION pressures.get_pressure_agriculture_country_pa(text, text, integer)
  OWNER TO h05ibex;
GRANT EXECUTE ON FUNCTION pressures.get_pressure_agriculture_country_pa(text, text, integer) TO public;
COMMENT ON FUNCTION pressures.get_pressure_agriculture_country_pa(text, text, integer) IS '...';